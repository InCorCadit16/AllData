﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AllData.WebAPI.Migrations
{
    public partial class initialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MoneySources",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 200, nullable: false),
                    Balance = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MoneySources", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Spents",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Amount = table.Column<float>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Comment = table.Column<string>(maxLength: 2500, nullable: true),
                    MoneySourceId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Spents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Spents_MoneySources_MoneySourceId",
                        column: x => x.MoneySourceId,
                        principalTable: "MoneySources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MoneySources_Name",
                table: "MoneySources",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_Spents_MoneySourceId",
                table: "Spents",
                column: "MoneySourceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Spents");

            migrationBuilder.DropTable(
                name: "MoneySources");
        }
    }
}
