﻿using AllData.Data.Entities;
using AllData.Model.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AllData.WebAPI.Controllers
{
    [ApiController]
    [Route("api/{controller}")]
    public class MoneySourceController: BaseController
    {

        private readonly MoneySourceService _moneySourceService;

        public MoneySourceController(MoneySourceService moneySourceService)
        {
            this._moneySourceService = moneySourceService;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<MoneySource[]>> GetMoneySources()
        {
            Response.Headers.Add("Access-Control-Allow-Origin", "http://localhost:4200");
            return Ok(await _moneySourceService.GetMoneySources());
        }
    }
}
