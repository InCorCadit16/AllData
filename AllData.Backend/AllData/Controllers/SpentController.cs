﻿using AllData.Model.Dtos;
using AllData.Model.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace AllData.WebAPI.Controllers
{
    [ApiController]
    [Route("api/{controller}")]
    public class SpentController: BaseController
    {
        private readonly SpentService _spentService;


        public SpentController(SpentService spentService)
        {
            this._spentService = spentService;
        }
        
        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<SpentDto[]>> GetAll()
        {
            return Ok(await _spentService.GetAll());
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> CreateSpent(CreateSpentDto createSpentDto)
        {
            await _spentService.CreateSpent(createSpentDto);
            return NoContent();
        }       
        
        [AllowAnonymous]
        [HttpPut]
        public async Task<IActionResult> UpdateSpent(SpentDto spentDto)
        {
            await _spentService.UpdateSpent(spentDto);
            return NoContent();
        }

        [AllowAnonymous]
        [HttpDelete("{spentId}")]
        public async Task<IActionResult> DeleteSpent(long spentId)
        {
            await _spentService.DeleteSpent(spentId);
            return Ok();
        }
    }
}                                                                                    