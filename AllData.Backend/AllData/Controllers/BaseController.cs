﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AllData.WebAPI.Controllers
{
    [Authorize]
    public class BaseController: ControllerBase
    {

    }
}
