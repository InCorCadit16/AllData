﻿using AllData.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace AllData.Data.Configs
{
    class SpentConfig : IEntityTypeConfiguration<Spent>
    {
        public void Configure(EntityTypeBuilder<Spent> builder)
        {
            builder.HasOne(s => s.MoneySource).WithMany(ms => ms.Spents).OnDelete(DeleteBehavior.Cascade);
        }
    }
}
