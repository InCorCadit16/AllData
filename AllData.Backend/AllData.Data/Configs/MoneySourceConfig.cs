﻿using AllData.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace AllData.Data.Configs
{
    class MoneySourceConfig : IEntityTypeConfiguration<MoneySource>
    {
        public void Configure(EntityTypeBuilder<MoneySource> builder)
        {
            builder.HasIndex(ms => ms.Name);
        }
    }
}
