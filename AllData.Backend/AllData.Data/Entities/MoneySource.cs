﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AllData.Data.Entities
{
    public class MoneySource: BaseEntity
    {
        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        [Range(0, int.MaxValue)]
        public float Balance { get; set; }

        public ICollection<Spent> Spents { get; set; }
    }
}
