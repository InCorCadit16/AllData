﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AllData.Data.Entities
{
    public class BaseEntity
    {
        public long Id { get; set; }
    }
}
