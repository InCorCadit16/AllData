﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AllData.Data.Entities
{
    public class Spent: BaseEntity
    {
        [Range(0,int.MaxValue)]
        public float Amount { get; set; }

        public DateTime Date { get; set; }

        [MaxLength(2500)]
        public string Comment { get; set; }

        public long MoneySourceId { get; set; }

        public MoneySource MoneySource { get; set; }
    }
}
