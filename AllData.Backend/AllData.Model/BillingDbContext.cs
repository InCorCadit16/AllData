﻿using AllData.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace AllData.Model
{
    public class BillingDbContext: DbContext
    {
        public DbSet<MoneySource> MoneySources { get; set; }

        public DbSet<Spent> Spents { get; set; }

        private readonly IConfiguration _configuration;

        public BillingDbContext(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_configuration.GetConnectionString("BillingDatabase"), builder => builder.MigrationsAssembly(_configuration.GetValue<string>("MigrationAssembly")));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.Load(_configuration.GetValue<string>("ConfigHolderAssembly")));
        }

        
    }
}
