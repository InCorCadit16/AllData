﻿using AllData.Data.Entities;
using AllData.Model.Dtos;
using AutoMapper;

namespace AllData.Model.MappingProfiles
{

    class MoneySourceProfile: Profile
    {
        public MoneySourceProfile()
        {
            this.CreateMap<MoneySource, MoneySourceDto>();
        }
    }
}
