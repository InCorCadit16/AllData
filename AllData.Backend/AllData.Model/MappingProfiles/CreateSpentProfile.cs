﻿using AllData.Data.Entities;
using AllData.Model.Dtos;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace AllData.Model.MappingProfiles
{
    class CreateSpentProfile: Profile
    {
        public CreateSpentProfile()
        {
            this.CreateMap<CreateSpentDto, Spent>();
        }
    }
}
