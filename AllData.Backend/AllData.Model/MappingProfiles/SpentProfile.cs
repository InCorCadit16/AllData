﻿using AllData.Data.Entities;
using AllData.Model.Dtos;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace AllData.Model.MappingProfiles
{
    class SpentProfile: Profile
    {
        public SpentProfile()
        {
            this.CreateMap<Spent, SpentDto>()
                .ForMember(sd => sd.MoneySourceName, mapper => mapper.MapFrom(s => s.MoneySource.Name))
                .ReverseMap();
        }
    }
}
