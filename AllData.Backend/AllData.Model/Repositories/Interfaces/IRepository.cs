﻿using AllData.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AllData.Model.Repositories.Implementations
{
    public interface IRepository
    {

        Task<TEntity[]> GetAll<TEntity>(params Expression<Func<TEntity, object>>[] includeProperties) where TEntity : BaseEntity;

        Task<TEntity> GetById<TEntity>(long id, params Expression<Func<TEntity, object>>[] includeProperties) where TEntity : BaseEntity;

        Task<TEntity> Insert<TEntity>(TEntity entity) where TEntity : BaseEntity;

        Task<TEntity> Update<TEntity>(TEntity entity) where TEntity : BaseEntity;

        Task<TEntity> Delete<TEntity>(long id) where TEntity : BaseEntity;

        //Task<PaginatorAnswer<TDto>> GetPage<TEntity, TDto>(PaginatorQuery paginatorQuery, params Expression<Func<TEntity, object>>[] includeProperties) where TDto : class;

        Task SaveAllChanges();

    }
}
