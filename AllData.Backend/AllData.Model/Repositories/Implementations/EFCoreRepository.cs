﻿using AllData.Data.Entities;
using AllData.Model.Repositories.Implementations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AllData.Model.Repositories.Interfaces
{
    public class EFCoreRepository : IRepository
    {
        private readonly BillingDbContext _context;

        public EFCoreRepository(BillingDbContext context)
        {
            this._context = context;
        }

        public async Task<TEntity[]> GetAll<TEntity>(params Expression<Func<TEntity, object>>[] includeProperties) where TEntity : BaseEntity
        {
            var query = IncludeProperties(includeProperties);
            return await query.ToArrayAsync();
        }

        public async Task<TEntity> GetById<TEntity>(long id, params Expression<Func<TEntity, object>>[] includeProperties) where TEntity : BaseEntity
        {
            var query = IncludeProperties(includeProperties);
            return await query.FirstOrDefaultAsync(t => t.Id == id);
        }

        public async Task<TEntity> Delete<TEntity>(long id) where TEntity : BaseEntity
        {
            var entity = await _context.Set<TEntity>().FindAsync(id);
            if (entity == null)
            {
                throw new KeyNotFoundException();
            }

            _context.Set<TEntity>().Remove(entity);
            await _context.SaveChangesAsync();

            return entity;
        }

        

        public async Task<TEntity> Insert<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            _context.Entry(entity).State = EntityState.Modified;
            await _context.AddAsync(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task SaveAllChanges()
        {
            await _context.SaveChangesAsync();
        }

        public async Task<TEntity> Update<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            _context.Entry(entity).State = EntityState.Modified;
            _context.Update(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        protected IQueryable<TEntity> IncludeProperties<TEntity>(params Expression<Func<TEntity, object>>[] includeProperties) where TEntity : BaseEntity
        {
            IQueryable<TEntity> entities = _context.Set<TEntity>();
            foreach (var includeProperty in includeProperties)
            {
                if (includeProperty.Body.ToString().Contains("."))
                {
                    string body = includeProperty.Body.ToString();
                    string property = body.Substring(body.IndexOf(".") + 1);
                    entities = entities.Include(property);
                }
                else
                    entities = entities.Include(includeProperty);
            }
            return entities;
        }
    }
}
