﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AllData.Model.Dtos
{
    public class SpentDto
    {
        public long Id { get; set; }

        public int Amount { get; set; }

        public DateTime Date { get; set; }

        public string Comment { get; set; }

        public long MoneySourceId { get; set; }

        public string MoneySourceName { get; set; }
    }
}
