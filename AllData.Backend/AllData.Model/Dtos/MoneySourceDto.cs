﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AllData.Model.Dtos
{
    public class MoneySourceDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public int Balance { get; set; }

        public ICollection<SpentDto> Spents { get; set; }
    }
}
