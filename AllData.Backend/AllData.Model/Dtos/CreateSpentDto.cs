﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AllData.Model.Dtos
{
    public class CreateSpentDto
    {
        public int Amount { get; set; }

        public DateTime Date { get; set; }

        public string Comment { get; set; }

        public long MoneySourceId { get; set; }
    }
}
