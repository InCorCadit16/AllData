﻿using AllData.Data.Entities;
using AllData.Model.Dtos;
using AllData.Model.Repositories.Implementations;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AllData.Model.Services
{
    public class MoneySourceService
    {
        private readonly IRepository _repository;
        private readonly IMapper _mapper;

        public MoneySourceService(IRepository repository, IMapper mapper)
        {
            this._repository = repository;
            this._mapper = mapper;
        }

        public async Task<MoneySourceDto[]> GetMoneySources()
        {
            var values = await _repository.GetAll<MoneySource>(ms => ms.Spents);


            return _mapper.Map<MoneySourceDto[]>(values);
        }
    }
}
