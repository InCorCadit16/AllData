﻿using AllData.Data.Entities;
using AllData.Model.Dtos;
using AllData.Model.Repositories.Implementations;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllData.Model.Services
{
    public class SpentService
    {
        private readonly IRepository _repository;
        private readonly IMapper _mapper;

        public SpentService(IRepository repository, IMapper mapper)
        {
            this._repository = repository;
            this._mapper = mapper;
        }

        public async Task<SpentDto[]> GetAll()
        {
            var result = await _repository.GetAll<Spent>(s => s.MoneySource);
            result = result.OrderByDescending(s => s.Date).ToArray();

            return _mapper.Map<SpentDto[]>(result);
        }

        public async Task<long> CreateSpent(CreateSpentDto createSpentDto)
        {
            var entity = _mapper.Map<Spent>(createSpentDto);

            entity = await _repository.Insert(entity);

            return entity.Id;
        }

        public async Task UpdateSpent(SpentDto spentDto)
        {
            var entity = _mapper.Map<Spent>(spentDto);
            entity.MoneySource = null;

            await _repository.Update(entity);
        }

        public async Task DeleteSpent(long spentId)
        {
            await _repository.Delete<Spent>(spentId);
        }
    } 
}
