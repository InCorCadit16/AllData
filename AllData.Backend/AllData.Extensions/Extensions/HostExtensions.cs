﻿using AllData.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AllData.Infrastructure.Extensions
{
    public static class HostExtensions
    {
        public static async Task SeedDb(this IHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;

                try
                {
                    var context = services.GetRequiredService<BillingDbContext>();
                    context.Database.Migrate();


                    await Seed.SeedMoneySources(context);
                    await Seed.SeedSpents(context);
                } 
                catch (Exception ex)
                {
                    throw new Exception("Error while seeding database", ex);
                }
            }
        }
    }
}
