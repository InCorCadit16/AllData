﻿using AllData.Data.Entities;
using AllData.Model;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllData.Infrastructure
{
    class Seed
    {

        public static async Task SeedMoneySources(BillingDbContext context)
        {
            if (!context.MoneySources.Any()) 
            {
                var card = new MoneySource
                {
                    Name = "Card",
                    Balance = 7254.12f
                };

                var wallet = new MoneySource
                {
                    Name = "Wallet",
                    Balance = 1950.50f
                };

                var bankAccount = new MoneySource
                {
                    Name = "Bank Account",
                    Balance = 12640
                };

                await context.AddAsync(card);
                await context.AddAsync(wallet);
                await context.AddAsync(bankAccount);
                await context.SaveChangesAsync();
            }
        }

        public static async Task SeedSpents(BillingDbContext context)
        {

            if (!context.Spents.Any())
            {
                var card = context.MoneySources.First(ms => ms.Name == "Card");
                var wallet = context.MoneySources.First(ms => ms.Name == "Wallet");
                var bankAccount = context.MoneySources.First(ms => ms.Name == "Bank Account");

                var spent1 = new Spent
                {
                    Amount = 250.37f,
                    Date = new DateTime(2020, 3, 23, 23, 44, 0),
                    Comment = "Some food in Grocery Store",
                    MoneySource = card
                };

                var spent2 = new Spent
                {
                    Amount = 137f,
                    Date = new DateTime(2020, 6, 17, 11, 5, 0),
                    Comment = "Pizza",
                    MoneySource = card
                };

                var spent3 = new Spent
                {
                    Amount = 1260f,
                    Date = new DateTime(2020, 4, 11, 19, 33, 0),
                    Comment = "New Iron",
                    MoneySource = card
                };

                var spent4 = new Spent
                {
                    Amount = 58f,
                    Date = new DateTime(2020, 4, 14, 23, 44, 0),
                    Comment = "Beer",
                    MoneySource = wallet
                };
                var spent5 = new Spent
                {
                    Amount = 532f,
                    Date = new DateTime(2020, 8, 23, 23, 44, 0),
                    Comment = "Dinner with family",
                    MoneySource = bankAccount
                };

                await context.AddAsync(spent1);
                await context.AddAsync(spent2);
                await context.AddAsync(spent3);
                await context.AddAsync(spent4);
                await context.AddAsync(spent5);
                await context.SaveChangesAsync();

            }
        }
    }
}
