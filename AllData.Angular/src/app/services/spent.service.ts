import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Spent } from '../data/models/spent.model';


@Injectable()
export class SpentService {

    constructor(private http: HttpClient) {
    }

    getAll() {
        return this.http.get<Spent[]>('api/spent');
    }

    createSpent(spent: Spent) {
        return this.http.post('api/spent', spent)
    }

    updateSpent(spent: Spent) {
        return this.http.put('api/spent', spent)
    }

    deleteSpent(spentId: number) {
        return this.http.delete(`api/spent/${spentId}`);
    }
 
}