import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MoneySource } from '../data/models/money-source.model';


@Injectable()
export class MoneySourceService {

    constructor(private http: HttpClient) {

    }

    getMoneySources() {
        return this.http.get<MoneySource[]>('api/moneySource')
    }
}