import { Component, OnInit } from '@angular/core';
import { SpentService } from 'src/app/services/spent.service';
import { Observable } from 'rxjs';
import { Spent } from 'src/app/data/models/spent.model';

@Component({
  selector: 'app-spent-list',
  templateUrl: './spent-list.component.html',
  styleUrls: ['./spent-list.component.css'],
  providers: [ SpentService ]
})
export class SpentListComponent implements OnInit {
  spents = new Observable<Spent[]>()

  constructor(private spentService: SpentService) {

  }

  ngOnInit(): void {
    this.spents = this.spentService.getAll();
  }

  refresh() {
    this.spents = this.spentService.getAll();
  }

}
