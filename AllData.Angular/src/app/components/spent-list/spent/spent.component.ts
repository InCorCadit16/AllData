import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SpentService } from 'src/app/services/spent.service';
import { MatDialog } from '@angular/material/dialog';
import { SpentEditComponent } from '../../spent-edit/spent-edit.component';

@Component({
  selector: 'app-spent',
  templateUrl: './spent.component.html',
  styleUrls: ['./spent.component.css'],
  providers: [ SpentService, MatDialog ]
})
export class SpentComponent implements OnInit {
  @Input() spent; 

  constructor(private spentService: SpentService,
              private dialog: MatDialog) { }

  ngOnInit(): void {
  }

  @Output() changed = new EventEmitter();

  onEdit() {
    let dialogRef = this.dialog.open(SpentEditComponent, {
      data: {spent: this.spent}
    });

    dialogRef.afterClosed()
    .subscribe(updatedSpent => {
      this.spentService.updateSpent(updatedSpent)
      .subscribe(
        success => { this.changed.emit(); },
        error => {  }
      )
    })
  }

  onDelete() {
    console.log(this.spent)
    this.spentService.deleteSpent(this.spent.id)
    .subscribe(
      success => { this.changed.emit(); },
      error => { }
    )
  }

}
