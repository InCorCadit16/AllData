import { Component, OnInit, Inject, ViewChild, AfterContentInit, AfterViewInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Spent } from 'src/app/data/models/spent.model';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { MoneySource } from 'src/app/data/models/money-source.model';
import { MoneySourceService } from 'src/app/services/money-source.service';
import { SpentService } from 'src/app/services/spent.service';
import { MatSelect } from '@angular/material/select';

@Component({
  selector: 'app-spent-edit',
  templateUrl: './spent-edit.component.html',
  styleUrls: ['./spent-edit.component.css'],
  providers: [ MoneySourceService, SpentService ]
})
export class SpentEditComponent implements OnInit, AfterViewInit {
  editSpentForm: FormGroup;

  constructor(public matDialogRef: MatDialogRef<SpentEditComponent>,
    private formBuilder: FormBuilder,
    private moneySourceService: MoneySourceService,
    private spentService: SpentService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.editSpentForm = this.formBuilder.group({
      date: [data.spent.date, [Validators.required]],
      amount: [data.spent.amount, [Validators.required, Validators.min(0)]],
      comment: [data.spent.comment, [Validators.maxLength(2500)]],
      moneySourceId: [data.spent.moneySourceId, [Validators.required]]
    })
  }

  moneySources = new Observable<MoneySource[]>();

  @ViewChild(MatSelect, { static:true }) sourceSelect: MatSelect;

  ngOnInit() {
    this.moneySources = this.moneySourceService.getMoneySources()
  }

  ngAfterViewInit() {
    this.sourceSelect.options.changes.subscribe(() => {
      this.sourceSelect.options.find(ms => ms.value == this.data.spent.moneySourceId).select();
    })
  }

  onSubmit() {
    let updatedSpent = {
      ...this.editSpentForm.value
    }

    updatedSpent.id = this.data.spent.id;

    this.matDialogRef.close(updatedSpent)

    
  }

  get date() {
    return this.editSpentForm.get("date");
  }

  get amount() {
    return this.editSpentForm.get("amount");
  }

  get comment() {
    return this.editSpentForm.get("comment");
  }

  get moneySourceId() {
    return this.editSpentForm.get("moneySourceId");
  }

}
