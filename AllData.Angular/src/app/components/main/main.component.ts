import { Component, OnInit, ViewChild } from '@angular/core';
import { SpentListComponent } from '../spent-list/spent-list.component';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  @ViewChild(SpentListComponent, {static : true}) spentList: SpentListComponent;
  @ViewChild(MatSidenav, {static:true}) sidenav: MatSidenav;


  constructor() { }

  ngOnInit(): void {
  }

  onCreate() {
    this.spentList.refresh();

    this.sidenav.toggle();
  }

}
