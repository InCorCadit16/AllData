import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { SpentService } from 'src/app/services/spent.service';
import { FormsModule, FormBuilder, Validators } from '@angular/forms';
import { MoneySourceService } from 'src/app/services/money-source.service';
import { MoneySource } from 'src/app/data/models/money-source.model';
import { Observable } from 'rxjs';
import { Spent } from 'src/app/data/models/spent.model';

@Component({
  selector: 'app-spent-create',
  templateUrl: './spent-create.component.html',
  styleUrls: ['./spent-create.component.css'],
  providers: [ SpentService, MoneySourceService ]
})
export class SpentCreateComponent implements OnInit {
  createSpentForm = this.formBuilder.group({
    date: [, [Validators.required]],
    amount: [, [Validators.required, Validators.min(0)]],
    comment: [, [Validators.maxLength(2500)]],
    moneySourceId: [, [Validators.required]]
  })

  moneySources = new Observable<MoneySource[]>();

  @Output() created = new EventEmitter(); 

  constructor(private formBuilder: FormBuilder,
              private moneySourceService: MoneySourceService,
              private spentService: SpentService) { }

  ngOnInit(): void {
    this.moneySources = this.moneySourceService.getMoneySources();
  }

  onSubmit() {
      let spent: Spent = {
        ...this.createSpentForm.value
      }

      this.spentService.createSpent(spent)
      .subscribe(
        success => {
          this.created.emit();
        },
        error => {

        }
      )
  }

  get date() {
      return this.createSpentForm.get("date");
  }

  get amount() {
      return this.createSpentForm.get("amount");
  }

  get comment() {
    return this.createSpentForm.get("comment");
  }

  get moneySourceId() {
    return this.createSpentForm.get("moneySourceId");
  }


}
