import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'

import { AppComponent } from './app.component';
import { SpentComponent } from './components/spent-list/spent/spent.component';
import { SpentListComponent } from './components/spent-list/spent-list.component'
import { SpentService } from './services/spent.service';
import { MainComponent } from './components/main/main.component';
import { AppRoutingModule } from './app-routing.module';
import { AppMaterialModule } from './app-material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SpentCreateComponent } from './components/spent-create/spent-create.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MoneySourceService } from './services/money-source.service';
import { SpentEditComponent } from './components/spent-edit/spent-edit.component';
import { MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule } from '@angular/material/dialog';

@NgModule({
  declarations: [
    AppComponent,
    SpentComponent,
    SpentListComponent,
    MainComponent,
    SpentCreateComponent,
    SpentEditComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    AppMaterialModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    MatDialogModule
  ],
  entryComponents:[
    SpentEditComponent
  ],
  providers: [
    SpentService,
    MoneySourceService,
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
