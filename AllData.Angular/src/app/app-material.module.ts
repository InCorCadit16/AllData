import { NgModule } from "@angular/core";
import { MatCardModule } from "@angular/material/card"
import { MatButtonModule } from "@angular/material/button"
import { MatListModule } from "@angular/material/list"
import { MatIconModule } from "@angular/material/icon"
import { MatSidenavModule } from "@angular/material/sidenav"
import { MatInputModule } from "@angular/material/input"
import { MatSelectModule } from '@angular/material/select'
import { MatDialogModule } from '@angular/material/dialog'

@NgModule({
    imports: [
        MatCardModule,
        MatButtonModule,
        MatListModule,
        MatIconModule,
        MatSidenavModule,
        MatInputModule,
        MatSelectModule,
        MatDialogModule,
    ],
    exports: [
        MatCardModule,
        MatButtonModule,
        MatListModule,
        MatIconModule,
        MatSidenavModule,
        MatInputModule,
        MatSelectModule,
        MatDialogModule,
    ]
})
export class AppMaterialModule {

}