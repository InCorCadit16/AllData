import { Spent } from './spent.model';


export class MoneySource {
    id: number;
    name: string;
    balance: number;
    spents: Spent[];
}