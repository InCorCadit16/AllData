

export class Spent {
    id: number;
    amount: number;
    date: Date;
    comment: string;
    moneySourceId: number;
    moneySourceName: string;
}