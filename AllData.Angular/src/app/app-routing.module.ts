import { Routes, RouterModule } from '@angular/router'
import { NgModule } from '@angular/core'

import { MainComponent } from './components/main/main.component'

const routes: Routes = [
 { path: 'main', component: MainComponent},
 { path: '', redirectTo: 'main', pathMatch: 'full'},
 { path: '**', redirectTo: 'main' }
]

@NgModule({
    declarations: [],
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [RouterModule]
})

export class AppRoutingModule { }